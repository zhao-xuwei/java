package jiudian;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * 管理员
 *
 */
public class AdministratorTest {
	public static void main(String[] args) {
		AdministratorTest t = new AdministratorTest();
		t.fun();
	}

	Scanner sc = new Scanner(System.in);

	ArrayList<GuestRoomModel> list1 = new ArrayList<>();// 客房
	ArrayList<ShopAssistantModel> list2 = new ArrayList<>();

	/**
	 * 管理员
	 */
	void fun() {
		while(true) {
			System.out.println("1-客房信息管理");
			System.out.println("2-营业员信息管理");
			System.out.println("0-结束程序");
			System.out.println("请输入功能编号");
			String x = sc.nextLine();
			if ("0".equals(x)) {
				System.out.println("程序已结束");
				break;
			}
			switch (x) {
			case "1":
				execute1();
				break;
			case "2":
				execute2();
				break;
			default:
				break;
			}

		}
	}

	/**
	 * 营业员信息管理
	 */
	void execute2() {
		while(true) {
			System.out.println("1-添加营业员信息");
			System.out.println("2-删除营业员信息");
			System.out.println("3-修改营业员信息");
			System.out.println("4-查询营业员信息");
			System.out.println("5-返回到主页");
			System.out.println("0-结束程序");
			System.out.println("请输入功能编号");
			String x = sc.nextLine();
			if ("0".equals(x)) {
				System.out.println("程序已结束");
				break;
			}
			switch (x) {
			case "1":
				add2();
				break;
			case "2":
				remove2();
				break;
			case "3":
				modification2();
				break;
			case "4":
				inquire2();
				break;
			case "5":
				fun();
				break;
			default:
				break;
			}
		}
	}

	// 查询营业员信息
	void inquire2() {
		System.out.println("请输入您要查询的姓名：");
		String s = sc.nextLine();
		for (int i = 0; i < list2.size(); i++) {
			if (s.equals(list2.get(i).getName())) {
				System.out.println("营业员姓名：" + list2.get(i).getName() + " ");
				System.out.println("职位：" + list2.get(i).getPosition());
				return;
			}
		}
		System.out.println("未查到此人");
	}

	// 修改营业员信息
	void modification2() {
		System.out.println("请输入姓名：");
		String s = sc.nextLine();
		for (int i = 0; i < list2.size(); i++) {
			if (s.equals(list2.get(i).getName())) {
				System.out.println("请输入修改后的姓名：");
				String name = sc.nextLine();
				list2.get(i).setName(name);
				System.out.println("请输入修改后的职位：");
				String position = sc.nextLine();
				list2.get(i).setPosition(position);
				System.out.println("修改成功");
				return;
			}
		}
		System.out.println("未查到此人");
	}

	// 删除营业员信息
	void remove2() {
		System.out.println("请输入要删除的姓名");
		String type = sc.nextLine();
		System.out.println("请输入要删除的职位：");
		String roomNumber = sc.nextLine();
		for (int i = 0; i < list2.size(); i++) {
			if (type.equals(list2.get(i).getName()) && roomNumber.equals(list2.get(i).getPosition())) {
				list2.remove(i);
				System.out.println("已删除");
				return;
			}
		}
		System.out.println("未查到此人");
	}

	// 添加营业员信息
	void add2() {
		while (true) {
			ShopAssistantModel sa = new ShopAssistantModel();
			System.out.println("请输入姓名：");
			String name = sc.nextLine();
			sa.setName(name);
			System.out.println("请输入职位：");
			String position = sc.nextLine();
			sa.setPosition(position);
			list2.add(sa);
			System.out.println("是否继续输入 y/n");
			String s = sc.nextLine();
			if ("n".equals(s)) {
				System.out.println("");
				break;
			}
		}
	}

//=======================================================

	/**
	 * 客房信息管理
	 */
	void execute1() {
		while (true) {
			System.out.println("1-添加客房信息");
			System.out.println("2-删除客房信息");
			System.out.println("3-修改客房信息");
			System.out.println("4-查询客房信息");
			System.out.println("5-返回到主页");
			System.out.println("0-结束程序");
			System.out.println("请输入功能编号");
			String x = sc.nextLine();
			if ("0".equals(x)) {
				System.out.println("程序已结束");
				break;
			}
			switch (x) {
			case "1":
				add1();
				break;
			case "2":
				remove1();
				break;
			case "3":
				modification1();
				break;
			case "4":
				inquire1();
				break;
			case "5":
				fun();
				break;
			default:
				break;
			}
		}
	}

	// 查询客房信息
	void inquire1() {
		System.out.println("请输入您要查询的房间号：");
		String s = sc.nextLine();
		for (int i = 0; i < list1.size(); i++) {
			if (s.equals(list1.get(i).getRoomNumber())) {
				System.out.println("房间号：" + list1.get(i).getRoomNumber() + " ");
				System.out.println("房间类型：" + list1.get(i).getType() + " ");
				System.out.println("房间状态：" + list1.get(i).getState());
				return;
			}
		}
		System.out.println("未查到此房间");
	}

	// 修改客房信息
	void modification1() {
		System.out.println("请输入房间号：");
		String s = sc.nextLine();
		for (int i = 0; i < list1.size(); i++) {
			if (s.equals(list1.get(i).getRoomNumber())) {
				System.out.println("请输入修改后的客房房型：");
				String type = sc.nextLine();
				list1.get(i).setType(type);
				System.out.println("请输入房间号：");
				String roomNumber = sc.nextLine();
				list1.get(i).setRoomNumber(roomNumber);
				System.out.println("请输入房间状态");
				String state = sc.nextLine();
				list1.get(i).setState(state);
				System.out.println("修改成功");
				break;
			}
		}

	}

	// 删除客房信息
	void remove1() {
		System.out.println("请输入要删除的房型");
		String type = sc.nextLine();
		System.out.println("请输入要删除的房间号：");
		String roomNumber = sc.nextLine();
		for (int i = 0; i < list1.size(); i++) {
			if (type.equals(list1.get(i).getType()) && roomNumber.equals(list1.get(i).getRoomNumber())) {
				list1.remove(i);
				System.out.println("已删除");
				return;
			}
		}
		System.out.println("未查到此房间");
	}

	// 添加客房信息
	void add1() {
		while (true) {
			GuestRoomModel gr = new GuestRoomModel();
			System.out.println("请输入客房房型：");
			String type = sc.nextLine();
			gr.setType(type);
			System.out.println("请输入房间号：");
			String roomNumber = sc.nextLine();
			gr.setRoomNumber(roomNumber);
			System.out.println("请输入房间状态");
			String state = sc.nextLine();
			gr.setState(state);
			list1.add(gr);
			System.out.println("是否继续输入 y/n");
			String s = sc.nextLine();
			if ("n".equals(s)) {
				System.out.println("");
				break;
			}
		}

	}

}
