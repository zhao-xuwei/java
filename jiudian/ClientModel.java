package jiudian;

/**
 * 客户注册信息
 *
 */
public class ClientModel {
	private String name;// 姓名
	private String idNumber;// 身份证号
	private String serialNumber;// 编号

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

}
