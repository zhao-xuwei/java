package jiudian;

import java.util.ArrayList;
import java.util.Scanner;

public class ClientTest {
	public static void main(String[] args) {
		
	}
	
	Scanner sc = new Scanner(System.in);
	ArrayList<ClientModel> list1 = new ArrayList<>();
	
	/**
	 * 客户信息管理
	 */
	void fun1(){
		while(true) {
			System.out.println("1-注册客户信息");
			System.out.println("2-修改客户信息");
			System.out.println("3-查看客户信息");
			System.out.println("0-结束程序");
			String x = sc.nextLine();
			if ("0".equals(x)) {
				System.out.println("程序已结束");
				break;
			}
			switch (x) {
			case "1":
				login();
				break;
			case "2":
				modification();
				break;
			case "3":
				look();
				break;
			default:
				break;
			}
		}
	}
	
	//查看
	void look() {
		System.out.println("请输入要查看的姓名：");
		String s = sc.nextLine();
		for(int i = 0; i < list1.size(); i++) {
			if(s.equals(list1.get(i).getName())) {
				System.out.print("姓名：" + list1.get(i).getName() + " ");
				System.out.print("身份证号" + list1.get(i).getIdNumber() + " ");
				System.out.print("编号：" + list1.get(i).getSerialNumber());
				return;
			}
		}
		System.out.println("未查到此人");
	}
	
	//修改
	void modification() {
		System.out.println("请输入姓名：");
		String s = sc.nextLine();
		for (int i = 0; i < list1.size(); i++) {
			if (s.equals(list1.get(i).getName())) {
				System.out.println("请输入修改后的姓名：");
				String name = sc.nextLine();
				list1.get(i).setName(name);
				System.out.println("请输入修改后的身份证号：");
				String idNumber = sc.nextLine();
				list1.get(i).setIdNumber(idNumber);
				System.out.println("请输入修改后的编号：");
				String serialNumber = sc.nextLine();
				list1.get(i).setSerialNumber(serialNumber);
				System.out.println("修改成功");
				return;
			}
		}
		System.out.println("未查到此人");
	}
	
	// 注册
	void login() {
		ClientModel client = new ClientModel();
		System.out.println("请输入姓名：");
		String name = sc.nextLine();
		client.setName(name);
		System.out.println("请输入身份证号");
		String idNumber = sc.nextLine();
		client.setIdNumber(idNumber);
		System.out.println("请输入编号：");
		String serialNumber = sc.nextLine();
		client.setSerialNumber(serialNumber);
		list1.add(client);
	}

}
