package jiudian;

/**
 * 客房
 *
 */
public class GuestRoomModel {
	private String type;// 房间类型
	private String roomNumber;// 房间号
	private String state;// 状态

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}
