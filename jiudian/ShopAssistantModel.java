package jiudian;

/**
 * 营业员
 *
 */
public class ShopAssistantModel {
	private String name;// 姓名
	private String position;// 职位

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

}
