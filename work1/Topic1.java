package work1;

import java.util.Scanner;

public class Topic1 {
	public static void main(String[] args) {
		Topic1 t = new Topic1();
		t.fun();
	}

	Scanner sc = new Scanner(System.in);

	void fun() {
		int a = Integer.parseInt(sc.nextLine());
		for (int i = 1; i < a; i++) {
			int sum = 0;
			for (int j = i; j < a; j++) {
				sum += j;
				if (sum == a) {
					for (int x = i; x <= j; x++) {
						System.out.print(x + " ");
					}
					System.out.println();
				}
			}
		}
	}

}
