package work1;

import java.util.Scanner;

public class Topic2 {
	public static void main(String[] args) {
		Topic2 t = new Topic2();
		t.judge();

	}

	Scanner sc = new Scanner(System.in);

	void judge() {
		System.out.println("请输入数字");
		String s = sc.nextLine();
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) != s.charAt(s.length() - 1 - i)) {
				System.out.println("对不起，您输入的数字不是回文数字");
				return;
			}
		}
		System.out.println(s + " 这个数是回文数字");
	}

}
