package work1;

public class Topic3 {
	public static void main(String[] args) {
		Topic3 t = new Topic3();
		t.fun();
	}

	void fun() {
		int i, j, k, m;
		for (i = 1; i < 10; i++) { 
			for (j = 0; j < 10; j++) { 
				k = i * 1000 + i * 100 + j * 10 + j;
				m = (int) Math.sqrt(k);// 开方，强制转换
				if (m * m == k) {
					System.out.println("这个数字是:");
					System.out.println(k + "=" + m + "*" + m);
				}
			}
		}
	}

}
