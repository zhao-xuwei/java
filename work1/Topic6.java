package work1;

import java.util.Scanner;

public class Topic6 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String str = sc.nextLine();
		int max = 0, count = 0, end = 0;
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) >= '0' && str.charAt(i) <= '9') {
				count++;
				if (max < count) {
					max = count;// 个数的最大值
					end = i;// 保存最后一个数的索引数
				}
			} else {
				count = 0;
			}
		}
		for (int j = end - max + 1; j <= end; j++) {
			System.out.print(str.charAt(j) + " ");
		}
	}

}
