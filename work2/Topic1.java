package work2;

import java.util.Scanner;

public class Topic1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("请输num1");
		String num1 = sc.nextLine();
		System.out.println("请输num2");
		String num2 = sc.nextLine();
		System.out.println("交换前：");
		System.out.println("num1:" + num1 + " " + "num2:" + num2);
		String x = num1;
		num1 = num2;
		num2 = x;
		System.out.println("交换后：");
		System.out.println("num1:" + num1 + " " + "num2:" + num2);
	}
}
