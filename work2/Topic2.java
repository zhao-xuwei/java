package work2;

import java.util.Scanner;

public class Topic2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("请输入单价：");
		double unitPrice = sc.nextDouble();
		System.out.println("请输入购买的数量：");
		double num = sc.nextDouble();
		System.out.println("用户输入金额：");
		double money = sc.nextDouble();
		double totalMoney = unitPrice * num;// 总价
		if (totalMoney >= 500) {// 打折
			totalMoney = totalMoney * 0.8;// 打折后的总价
		}

		if (money >= totalMoney) {
			System.out.println("应收金额为：" + totalMoney + "元");
			double change = money - totalMoney;// 找零
			System.out.println("找零：" + change + "元");
		}

		if (money < totalMoney) {
			System.out.println("金额不足");
		}

	}
}
