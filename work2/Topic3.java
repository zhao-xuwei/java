package work2;

import java.util.ArrayList;
import java.util.Scanner;

import org.junit.Test;

public class Topic3 {

	Scanner sc = new Scanner(System.in);

	@Test
	public void fun0() {
		System.out.println("请输入年份");
		int yesr = sc.nextInt();
		System.out.println("请输入月份");
		int month = sc.nextInt();
		if (yesr % 4 == 0 && yesr % 100 != 0) {
			if (month == 2) {
				System.out.println("2月份有29天");
			}
		} else if (yesr % 400 == 0) {
			if (month == 2) {
				System.out.println("2月份有29天");
			}
		} else {
			System.out.println("2月份有28天");
		}

		switch (month) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			System.out.println(month + "月份有31天");
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			System.out.println(month + "月份有30天");
		default:
			break;
		}
	}

	@Test
	public void fun1() {
		for (int i = 1; i <= 5; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print(j);
			}
			System.out.println();
		}
	}

	@Test
	public void fun2() {
		for (int i = 1; i <= 6; i++) {// 行数
			for (int j = 5; j >= i; j--) {// 输出空格
				System.out.print(" ");
			}

			for (int k = 1; k <= i * 2 - 1; k++) {// 输出“*”
				System.out.print("*");
			}
			System.out.println();
		}
	}

	@Test
	public void fun3() {
		for (int i = 1; i <= 6; i++) {// 行数
			for (int j = 1; j < i; j++) {// 输出空格
				System.out.print(" ");
			}

			for (int k = 11; k >= i * 2 - 1; k--) {// 输出“*”
				System.out.print("*");
			}
			System.out.println();
		}
	}

	@Test
	public void fun4() {
		int sum1 = 0;
		int sum2 = 0;
		for (int i = 1; i <= 1000; i++) {
			if (i % 2 == 0) {// 判断是否为偶数
				sum1 += i;
			}

			if (i % 2 != 0) {// 判断是否为奇数
				sum2 += i;
			}
		}
		System.out.println("1—1000之间所有的偶数和为：" + sum1);
		System.out.println("1—1000之间所有的奇数和为：" + sum2);
	}

	@Test
	public void fun5() {
		// 用for循环输出1—1000之间能被5整除的数，且每行输出3个
		ArrayList<Integer> list = new ArrayList<>();
		// 找出能被5整除的数，放入集合。
		for (int i = 1; i <= 1000; i++) {
			if (i % 5 == 0) {
				list.add(i);
			}
		}
		
		for (int i = 0; i < list.size(); i++) {
			System.out.print(list.get(i) + " ");
			if ((i + 1) % 3 == 0) {// 每3个一行
				System.out.println();
			}
		}
	}

	@Test
	public void fun6() {
		// 9的阶乘
		int sum = 1;
		for (int i = 1; i <= 9; i++) {
			sum *= i;
		}
		System.out.println(sum);
	}

	@Test
	public void fun7() {
		// 使用循环打印100-200之间所有的素数（只能被1和自己整除的数叫素数）
		int x = 0;
		for (int i = 100; i <= 200; i++) {// 遍历100-200之间的数
			for (int j = 1; j <= i; j++) {
				if (i % j == 0) {// 判断是否为质数
					x++;// 是质数x只加2次
				}
			}

			if (x == 2) {
				System.out.println(i);
			}
			x = 0;
		}

	}

}
