package work3;

/**
 * 1、int[] scores={0,0,1,2,3,5,4,5,2,8,7,6,9,5,4,8,3,1,0,2,4,8,7,9,5,2,1,2,3,9};
 * 求出上面数组中0-9分别出现的次数 双重for循环
 *
 */
public class Topic1 {
	public static void main(String[] args) {
		int[] scores = { 0, 0, 1, 2, 3, 5, 4, 5, 2, 8, 7, 6, 9, 5, 4, 8, 3, 1, 0, 2, 4, 8, 7, 9, 5, 2, 1, 2, 3, 9 };
		int time = 0;
		for (int i = 0; i <= 9; i++) {
			for (int j = 0; j < scores.length; j++) {
				if (i == scores[j]) {
					time++;
				}
			}
			System.out.println(i + "出现的次数为：" + time);
			time = 0;
		}
	}
}
