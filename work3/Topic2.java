package work3;

/**
 * 2、int[] scores={0,0,1,2,3,5,4,5,2,8,7,6,9,5,4,8,3,1,0,2,4,8,7,9,5,2,1,2,3,9};
 * 要求求出其中的奇数个数和偶数个数。
 *
 */
public class Topic2 {
	public static void main(String[] args) {
		int[] scores = { 0, 0, 1, 2, 3, 5, 4, 5, 2, 8, 7, 6, 9, 5, 4, 8, 3, 1, 0, 2, 4, 8, 7, 9, 5, 2, 1, 2, 3, 9 };
		int num1 = 0;
		int num2 = 0;
		for (int i = 0; i < scores.length; i++) {
			if (scores[i] % 2 == 0) {
				num1++;
			}

			if (scores[i] % 2 != 0) {
				num2++;
			}
		}
		System.out.println("偶数的个数为：" + num1);
		System.out.println("奇数的个数为：" + num2);
	}
}
