package work3;

import java.util.Scanner;

/**
 * 3、输入一组学生的成绩，使用数组，然后计算他们的平均值. int[] scores = new int[3];
 *
 */
public class Topic3 {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int[] scores = new int[3];
		System.out.println("请输入第一个学生的成绩：");
		scores[0] = sc.nextInt();
		System.out.println("请输入第二个学生的成绩：");
		scores[1] = sc.nextInt();
		System.out.println("请输入第三个学生的成绩：");
		scores[2] = sc.nextInt();
		int len = scores.length;// 学生的个数
		int num = 0;// 成绩总和
		int meanValue;// 平均值
		
		for(int i = 0; i < len; i++) {
			num += scores[i];
		}
		
		meanValue = num / len;
		System.out.println("成绩平均值为：" + meanValue);
	}
}
