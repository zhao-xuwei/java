package work3;

/**
 * 输入一行字符，分别统计出其中英文字母、空格、数字和其它字符的个数。
 *
 */
public class Topic5 {
	public static void main(String[] args) {
		char[] array = new char[] { ' ', 'a', 's', '4', ' ', 's', '$', '4', 's', '6', 'f', '4', 's', '6', 'f' };
		int letter = 0;// 字母的数量
		int num = 0;// 数字的数量
		int blank = 0;// 空格的数量
		int rests = 0;// 其他字符的数量
		for (int i = 0; i < array.length; i++) {
			if (array[i] >= 'A' && array[i] <= 'Z') {
				letter++;
			} else if (array[i] >= 'a' && array[i] <= 'z') {
				letter++;
			} else if (array[i] >= '0' && array[i] <= '9') {
				num++;
			} else if (array[i] == ' ') {
				blank++;
			} else {
				rests++;
			}
		}
		System.out.println(letter);
		System.out.println(num);
		System.out.println(blank);
		System.out.println(rests);
	}
}
