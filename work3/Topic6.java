package work3;

import java.util.Scanner;

/**
 * 18、题目：企业发放的奖金根据利润提成。利润(I)低于或等于10万元时，奖金可提10%；利润高于10万元，低于20万元时，低于10万元的部分按10%提
 * 成，高于10万元的部分，可可提成7.5%；20万到40万之间时，高于20万元的部分，可提成5%；40万到60万之间时高于40万元的部分，可提成3%；60万
 * 到100万之间时，高于60万元的部分，可提成1.5%，高于100万元时，超过100万元的部分按1%提成，从键盘输入当月利润，求应发放奖金总数？
 */
public class Topic6 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("请输入利润（w）：");
		double profit = sc.nextDouble();
		double totalSum = 0;// 总金额
		if (profit <= 10) {
			totalSum = profit + profit * 0.1;
		}

		
		if (profit > 10 && profit <= 20) {
			totalSum = profit + 10 * 0.1 + (profit - 10) * 0.075;
		}

		if (profit > 20 && profit <= 40) {
			totalSum = profit + (profit - 20) * 0.05;
		}

		if (profit > 40 && profit <= 60) {
			totalSum = profit + (profit - 40) * 0.03;
		}

		if (profit > 60 && profit <= 100) {
			totalSum = profit + (profit - 60) * 0.015;
		}

		if (profit > 100) {
			totalSum = profit + (profit - 40) * 0.01;
		}

		System.out.println("应发放奖金总数:" + totalSum + "w");
	}
}
